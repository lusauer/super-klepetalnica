/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  
  var sporocilo = vhodnoBesedilo;
  var regularniIzraz = new RegExp (/\b(http\S+\.(?:jpg|png|gif))\b/g);
  
  console.log("vhodno besedilo je: "+sporocilo);
  
  var urlji = sporocilo.match(regularniIzraz);
  var tabela=["wink.png","smiley.png","like.png","kiss.png","sad.png"];
  
  for(var i = 0; i < urlji.length ;i++ ){
    var flag = true;
    for(var j = 0; j<5; j++){
      if(urlji[i].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/"+tabela[j])>-1){
        flag = false;
      }
    }
    
    if(flag){
    sporocilo=sporocilo+"</br><img src="+urlji[i]+" width='200' style='padding-left:20px;'>";
    }
  }
  console.log("izhodno besedilo je: "+sporocilo);
  
  return sporocilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
   if (sporocilo.charAt(0) != ('(')) {
   
    var ime = sporocilo.substr(0, Math.min(sporocilo.indexOf(' '), sporocilo.indexOf(':')));
    var zamenjava = '';
    
    for (var i = 0; i < vsiUporabniki.length; i++) {

      if (vsiUporabniki[i] == ime) {
        zamenjava = vsiNadimki[i];
      }
    }
    
    if (zamenjava.length > 0) {
      sporocilo = sporocilo.replace(ime, zamenjava+' ('+ime+')');
    }
  }
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var SimbolHtml = sporocilo.indexOf("&#") > -1;
  if (jeSmesko) {
    
    var jeSlika = sporocilo.indexOf("http") > -1;
    
    if(jeSmesko && !jeSlika){
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
    }
  
  
  else if(SimbolHtml){
    return divElementHtmlTekst(sporocilo);
  }
 
  else if (jeSlika){
    return divElementHtmlTekst(sporocilo);
  }
  else{

    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSlike(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  socket.on('spremembaVzdevka', function(rezultat){
  for(var i=0; i < vsiUporabniki.length ;i++){

    if(vsiUporabniki[i]==rezultat.stariVzdevek){
      vsiUporabniki[i]=rezultat.noviVzdevek;
    }
  }
});
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
 
 
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
  
   
   
   for (var i=0; i < uporabniki.length; i++) {

     var zeObstaja = false;
      
      for (var j=0; j < vsiUporabniki.length; j++) {
        if (vsiUporabniki[j] == uporabniki[i]) {
          zeObstaja = true;
          break;
        }
      }
      if(zeObstaja){
       $('#seznam-uporabnikov').append(divElementEnostavniTekst(vsiNadimki[j]+' ('+uporabniki[i]+')'));
      }
      else if(!zeObstaja){
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
      
    }
     
    $('#seznam-uporabnikov div').click(function(){
      klepetApp.procesirajUkaz('/zasebno "' +  $(this).text()+ '" "&#9756;"');
      $('#sporocila').append(divElementHtmlTekst('(zasebno za '+$(this).text() +'): '+ '&#9756;'));
      $('#poslji-sporocilo').focus();
    });
  });
 
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
