// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika
var vsiUporabniki = [];
var vsiNadimki = [];
var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        var katera=0;
       
        var zeObstajaNadimek = false;
        
        for(var i =0; i< vsiUporabniki.length; i++){
          if(vsiUporabniki[i]== parametri[1]){
            zeObstajaNadimek=true;
            katera=i;
            i=vsiUporabniki.length;
          }
        }
        if(zeObstajaNadimek){
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + parametri[1] +' ('+vsiNadimki[zaporedna]+')): ' + parametri[3];
        }
        else if(!zeObstajaNadimek){
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
        }
        
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;

      case 'barva':
        var imeBarve = besede[1];
        document.documentElement.style.setProperty(`--barvaSporocila`, imeBarve);
        document.documentElement.style.setProperty(`--barvaKanala`, imeBarve);
        document.getElementById('kanal').style.backgroundColor = imeBarve;
        document.getElementById('sporocila').style.backgroundColor = imeBarve;
        break;

      case 'preimenuj':
         var jeZe=false;
       
      besede.shift();
    
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      
      if (parametri) {
        var i;
        for (i=0; i < vsiUporabniki.length; i++) {
          if (vsiUporabniki[i] == parametri[1]) {
            vsiNadimki[i] == parametri[3];

            zeObstaja = true;
          }
        }
      
        if (!jeZe) {
          vsiUporabniki[vsiUporabniki.length] = parametri[1];
          vsiNadimki[vsiNadimki.length] = parametri[3];
        }
        sporocilo ='Za uporabnika ' +parametri[1]+' dodan nadimek '+parametri[3] + '.';
      } else {
        sporocilo = 'Neznan ukaz';
      }
        
    break;
>>>>>>> preimenovanje
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};